﻿##### https://gitlab.com/SheriffTexer/windows-setup

$env:PSTools = [System.Environment]::GetEnvironmentVariable('PSTools', 'User')
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'
$url_profile_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/profile.ps1'

Function Is-Administrator  
{  
    $user = [Security.Principal.WindowsIdentity]::GetCurrent();
    return (New-Object Security.Principal.WindowsPrincipal $user).IsInRole([Security.Principal.WindowsBuiltinRole]::Administrator)
}

Function Read-EnvironmentVariables
{
    If (Is-Administrator)
    {
        $hive = 'Machine'
    }
    Else
    {
        $hive = 'User'
    }

    $Tools = [System.Environment]::GetEnvironmentVariable('Tools', $hive)
    $PSTools = [System.Environment]::GetEnvironmentVariable('PSTools', $hive)
    If (-not $Tools)
    {
        throw "Environment variable 'Tools' in hive $hive is missing!"
    }
    
    If (-not $PSTools)
    {
        throw "Environment variable 'PSTools' in hive $hive is missing!"
    }
    return $Tools,$PSTools
}


Function Update-HelperFunctions  
{  
    $path_to_helper_functions_script = Join-Path -Path $env:PSTools -ChildPath 'helper_functions.ps1'
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}

Function Update-Profile  
{  
    New-Item -ItemType File -Path $PROFILE.CurrentUserAllHosts -Force
    Invoke-WebRequest -Uri $url_profile_script -OutFile $PROFILE.CurrentUserAllHosts
}


Function Get-ThisScriptDirectory
{
    if ($psise) {
        Split-Path $psise.CurrentFile.FullPath
    }
    else {
        $global:PSScriptRoot
    }
}

# https://stackoverflow.com/questions/2434133/progress-during-large-file-copy-copy-item-write-progress
Function Copy-WithProgress
{
    [CmdletBinding()]
    Param
    (
        [Parameter(Mandatory=$true,
            ValueFromPipelineByPropertyName=$true)]
        $Source,
        [Parameter(Mandatory=$true,
            ValueFromPipelineByPropertyName=$true)]
        $Destination
    )

    $Source=$Source.tolower()
    $Filelist=Get-Childitem "$Source" –Recurse
    $Total=$Filelist.count
    $Position=0

    foreach ($File in $Filelist)
    {
        $Filename=$File.Fullname.tolower().replace($Source,'')
        $DestinationFile=($Destination+$Filename)
        Write-Progress -Activity "Copying data from '$source' to '$Destination'" -Status "Copying File $Filename" -PercentComplete (($Position/$total)*100)
        Copy-Item $File.FullName -Destination $DestinationFile
        $Position++
    }
}

Function Print-StatusInfo
{
    [cmdletbinding()]
    Param (
        [Parameter(Mandatory)]
        [String]$Info,
        [ValidateSet('[+]','[>]','[!]','[ok]')]
		[String]$Symbol = '[+]'
    )

    If ($Symbol -eq '[!]')
    {
        $SymbolColor = 'Red'
        $InfoColor = 'Yellow'
    }
    ElseIf ($Symbol -eq '[ok]')
    {
        $SymbolColor = 'Green'
        $InfoColor = 'White'
    }
    Else
    {
        $SymbolColor = 'Cyan'
        $InfoColor = 'Magenta'
    }
    Write-Host "$Symbol " -ForegroundColor $SymbolColor -NoNewline
    Write-Host "$Info" -ForegroundColor $InfoColor
}


Function Print-Banner
{
    [cmdletbinding()]
    Param (
        [Parameter(Mandatory)]
        [String]$Info,
        [ValidateSet('+','#')]
		[String]$Symbol = '#'
    )

    Write-Host $("$Symbol" * ($Info.Length + 4)) -ForegroundColor Cyan
    Write-Host "$Symbol " -ForegroundColor Cyan -NoNewline
    Write-Host "$Info " -ForegroundColor Magenta -NoNewline
    Write-Host "$Symbol" -ForegroundColor Cyan
    Write-Host $("$Symbol" * ($Info.Length + 4)) -ForegroundColor Cyan
}


Function WaitFor-UserInput
{
    [cmdletbinding()]
    Param (
        [Parameter(Mandatory)]
        [String]$Info,
        [Parameter(Mandatory)]
        [String]$UserInput,
        [ValidateSet('[+]','[>]','[!]')]
		[String]$Symbol = '[+]'
    )

    Write-Host "$Symbol " -ForegroundColor Cyan -NoNewline
    Write-Host "User Input required:" -ForegroundColor Magenta
    Write-Host "    $Info" -ForegroundColor Cyan
    Write-Host "    $UserInput" -ForegroundColor Magenta
}


Function Create-Shortcut
{
    [cmdletbinding()]
    Param (
        [Parameter(Mandatory)]
        [String]$Name,
        [Parameter(Mandatory)]
        [String]$Executable,
        [Parameter(Mandatory)]
        [ValidateSet('Desktop')]
        [String]$TargetFolder = 'Desktop',
        [Parameter(ValueFromRemainingArguments=$true)]
        [String[]]$Arguments
    )
    
    If (!(Test-Path -Path $Executable -PathType Leaf))
    {
        If (!(Test-Path -Path $Executable -PathType Container))
        {
            Print-StatusInfo -Info "File/Folder not found: $Executable" -Symbol '[!]'
            Return
        }

    }
    $WScriptShell = New-Object -ComObject WScript.Shell
    $ShortcutPath = (Join-Path -Path $WScriptShell.SpecialFolders($TargetFolder) -ChildPath ($Name)) + '.lnk'

    If (Test-Path -Path $ShortcutPath -PathType Leaf)
    {
        Print-StatusInfo -Info "Shortcut already exists: $ShortcutPath" -Symbol '[ok]'
        Return
    }


    $Shortcut = $WScriptShell.CreateShortcut($ShortcutPath)
    $Shortcut.TargetPath = $Executable
    If ($Arguments)
    {
        Foreach ($Argument in $Arguments)
        {
            $Shortcut.Arguments = $Shortcut.Arguments + $Argument + ' '
        }
    }
    $Shortcut.Description = $Name
    $Shortcut.Save()
}


Function Read-Certificates-For-ReportingTool
{
    [cmdletbinding()]
    Param (
        [String]$Path = (Join-Path -Path $HOME -ChildPath "Downloads")
    )

    Print-StatusInfo -Info "Reading Certificates for Reporting Tool from: $Path"
    
    $AllSubjects = @()
    Get-ChildItem -Path $Path -Recurse -Include *.der | ForEach-Object {
        $Cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($_)
        $SubjectDict = @{}
        $Cert.Subject -split ", " | ForEach-Object {
            $Key,$Value = $_.Split("=")
            # Populate $SubjectDict
            $SubjectDict[$Key] = $Value
        }
        $AllSubjects += $SubjectDict
    }

    ForEach ($Subject in $AllSubjects) {
        If ($Subject.ContainsKey('C'))
        {
            # new certificate type (contains country, mail, etc.)
            Write-Output ("/C=" + $Subject['C'] + "/ST=" + $Subject['S'] + "/O=" + $Subject['O'] + "/emailAddress=" + $Subject['E'] + "/serialNumber=" + $Subject['SERIALNUMBER'] + "/GN=" + $Subject['G'] + "/SN=" + $Subject['SN'] + "/CN=" + $Subject['CN'] + ":xxj31ZMTZzkVA")
        }
        Else
        {
            # old certificate type
            Write-Output ("/serialNumber=" + $Subject['SERIALNUMBER'] + "/GN="+ $Subject['G'] + "/SN="+ $Subject['SN'] + "/O="+ $Subject['O'] + "/CN="+ $Subject['CN'] + ":xxj31ZMTZzkVA")
        }
    }
}


function Download-Appx-From-Store
{
    [CmdletBinding()]
    param
    (
      [Parameter(Mandatory)]
      [String]$Url,
      [String]$Path = $env:TEMP
    )
   
    process
    {
        If (-Not (Test-Path $Path))
        {
            New-Item -ItemType Directory -Force -Path $Path
        }
        
        # Get URLs of Appx to download
        $WebResponse = Invoke-WebRequest -UseBasicParsing -Method 'POST' -Uri 'https://store.rg-adguard.net/api/GetFiles' -Body "type=url&url=$Url&ring=Retail" -ContentType 'application/x-www-form-urlencoded'
        $LinksMatch = ($WebResponse.Links | where {$_ -like '*.appx*'} | where {$_ -like '*_neutral_*' -or $_ -like "*_"+$env:PROCESSOR_ARCHITECTURE.Replace("AMD","X").Replace("IA","X")+"_*"} | Select-String -Pattern '(?<=a href=").+(?=" r)').matches.value
        $FileNames = ($WebResponse.Links | where {$_ -like '*.appx*'} | where {$_ -like '*_neutral_*' -or $_ -like "*_"+$env:PROCESSOR_ARCHITECTURE.Replace("AMD","X").Replace("IA","X")+"_*"} | where {$_ } | Select-String -Pattern '(?<=noreferrer">).+(?=</a>)').matches.value

        If ($LinksMatch -is [array])
        {
            #for($i = 0;$i -lt $LinksMatch.Count; $i++)
            #{
            #    $Array += ,@($LinksMatch[$i], $FileNames[$i])
            #}
            Print-StatusInfo -Info "Found multiple Files in Store" -Symbol '[!]'
            foreach ($file in $FileNames)
            {
                Write-Host $file
            }
            Return
        }
        Else
        {
            # only one link found
            #$Array += ,@($LinksMatch, $FileNames)
            
            $FilePath = Join-Path -Path $Path -ChildPath $FileNames
            $FileRequest = Invoke-WebRequest -Uri $LinksMatch -UseBasicParsing #-Method Head
            [System.IO.File]::WriteAllBytes($FilePath, $FileRequest.content)
            Write-Host "File downloaded: $FilePath"
            
            Return $FilePath
        }

        #for($i = 0;$i -lt $Array.Count; $i++)
        #{
        #    $CurrentFile = $Array[$i][1]
        #    $CurrentUrl = $Array[$i][0]

        #    $FilePath = Join-Path -Path $Path -ChildPath $CurrentFile
        #    $FileRequest = Invoke-WebRequest -Uri $CurrentUrl -UseBasicParsing #-Method Head
        #    [System.IO.File]::WriteAllBytes($FilePath, $FileRequest.content)
        #    Write-Output "File downloaded: $FilePath"
        #    $DownloadedPackages += ,$FilePath
        #}

        #Return $DownloadedPackages
    }
}


function Disable-WSL-PortForwarding
{
    Print-StatusInfo -Info 'Removing v4tov4 Port Proxies'
    netsh interface portproxy reset ipv4
    Print-StatusInfo -Info 'Removing WSL 2 Firewall Port Forwarding Rules'
    Remove-NetFireWallRule -DisplayName 'WSL 2 Firewall Port Forwarding' -ErrorAction SilentlyContinue
}



function Enable-WSL-PortForwarding
{
    [CmdletBinding()]
    param
    (
      [Parameter(Mandatory)]
      [String[]]$Ports,
      [String]$ListeningAddress = "0.0.0.0"
    )

    Disable-WSL-PortForwarding

    wslconfig /setdefault kali-linux

    # $KaliIP = bash.exe -c "ifconfig eth0 | grep 'inet ' | awk -F ' +' '{print $3}'" # does NOT work in PowerShell
    $ifconfig = bash.exe -c "ifconfig eth0 | grep 'inet '"
    $found = $ifconfig -match '\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}';

    If( $found )
    {
      $KaliIP = $matches[0];
    }
    Else
    {
      Print-StatusInfo -Info "IP address of WSL 2 cannot be found! Please check WSL and try again." -Symbol '[!]'
      Return
    }

    For( $i = 0; $i -lt $Ports.length; $i++ )
    {
      $Port = $Ports[$i];
      Print-StatusInfo -Info ("Adding v4tov4 Port Proxy: "+$ListeningAddress+":$Port > "+$KaliIP+":$Port")
      #netsh interface portproxy delete v4tov4 listenport=$Port listenaddress=$Addr
      netsh interface portproxy add v4tov4 listenport=$Port listenaddress=$ListeningAddress connectport=$Port connectaddress=$KaliIP
    }

    # Add new WSL Firewall Port Forwarding Rules for Inbound and Outbound Traffic
    Print-StatusInfo -Info ("Adding new Inbound/Outbound Firewall Rules 'WSL 2 Firewall Port Forwarding': $Ports")
    New-NetFireWallRule -DisplayName 'WSL 2 Firewall Port Forwarding' -Direction Outbound -LocalPort $Ports -Action Allow -Protocol TCP > $null
    New-NetFireWallRule -DisplayName 'WSL 2 Firewall Port Forwarding' -Direction Inbound -LocalPort $Ports -Action Allow -Protocol TCP > $null
}


