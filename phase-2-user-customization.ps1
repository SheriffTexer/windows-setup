﻿##### https://gitlab.com/SheriffTexer/windows-setup

#Requires -RunAsAdministrator

##### Variables
$PSTools = "D:\ToolsWindows\PowerShellTools\"
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'

##### Import Helper Functions
$path_to_helper_functions_script = Join-Path -Path $PSTools -ChildPath 'helper_functions.ps1'
If (!(Test-Path $path_to_helper_functions_script -PathType Leaf))
{
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}
. $path_to_helper_functions_script


##### Read Environment Variables
$env:Tools, $env:PSTools = Read-EnvironmentVariables


Print-Banner -Info 'Windows OS Setup - Phase 2: User Customization'


##### PowerShell 5: Profile
Print-StatusInfo -Info 'Creating PowerShell 5 Profile'
New-Item -ItemType File -Path $PROFILE.CurrentUserAllHosts -Force
Invoke-WebRequest -Uri 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/profile.ps1' -OutFile $PROFILE.CurrentUserAllHosts


##### PowerShell Core: Profile
Print-StatusInfo -Info 'Creating PowerShell Core Profile'
New-Item -ItemType File -Path 'D:\OneDrive - Siemens AG\Documents\PowerShell\profile.ps1' -Force
Invoke-WebRequest -Uri 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/profile.ps1' -OutFile 'D:\OneDrive - Siemens AG\Documents\PowerShell\profile.ps1'


##### Drive Names
Print-StatusInfo -Info 'Setting Drive Names'
Get-CimInstance -ClassName Win32_Volume -Filter "DriveLetter = 'C:'" | Set-CimInstance -Property @{Label = 'System'}
Get-CimInstance -ClassName Win32_Volume -Filter "DriveLetter = 'D:'" | Set-CimInstance -Property @{Label = 'Data'}


##### Dark Mode
Print-StatusInfo -Info 'Activating Dark Mode'
Set-ItemProperty -Path HKCU:\SOFTWARE\Microsoft\Windows\CurrentVersion\Themes\Personalize -Name AppsUseLightTheme -Value 0


##### Show hidden Files
Print-StatusInfo -Info 'Showing hidden Files in Explorer'
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name Hidden -Value 1


##### Enable File Name Extensions
Print-StatusInfo -Info 'Enabling File Name Extensions in Explorer'
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name HideFileExt -Value 0


##### Hide News and Interests Bar in Taskbar
Print-StatusInfo -Info 'Hide News and Interests Bar in Taskbar'
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Feeds -Name ShellFeedsTaskbarViewMode -Value 2


##### Show all Icons in Notification Area
Print-StatusInfo -Info 'Showing all Icons in the Notification Area'
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer -Name EnableAutoTray -Value 0


##### Never combine Taskbar Buttons
Print-StatusInfo -Info 'Never combining Taskbar Buttons'
Set-ItemProperty -Path HKCU:\Software\Microsoft\Windows\CurrentVersion\Explorer\Advanced -Name TaskbarGlomLevel -Value 2

##### Restart Explorer for the Changes to take effect
Print-StatusInfo -Info 'Restarting Explorer'
Stop-Process -processName: Explorer -Force


##### Hybrid Azure AD Join
# https://wiki.siemens.com/pages/viewpage.action?pageId=213489630
Print-StatusInfo -Info 'Joining Hybrid Azure AD'
New-Item -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CDJ\AAD -Force
Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CDJ\AAD -Name TenantId -Value 38ae3bcd-9579-4fd4-adda-b42e1495d55a -Type String
Set-ItemProperty -Path HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\CDJ\AAD -Name TenantName -Value siemens.com -Type String

##### Mapping Network Shares
Print-StatusInfo -Info 'Mapping Network Shares'
New-PSDrive -Name P -Root \\ad001.siemens.net\dfs001\File\PT\Encrypted_Pentesting -PSProvider FileSystem -Scope 'Global' -Persist
New-PSDrive -Name S -Root \\ad001.siemens.net\dfs001\file\DE\SET -PSProvider FileSystem -Scope 'Global' -Persist
New-PSDrive -Name T -Root \\ad001.siemens.net\dfs001\file\DE\STT-Delegation -PSProvider FileSystem -Scope 'Global' -Persist
New-PSDrive -Name U -Root \\defthwa9dw3srv.ad001.siemens.net\umw-apps$ -PSProvider FileSystem -Scope 'Global' -Persist
New-PSDrive -Name X -Root \\ad001.siemens.net\dfs001\file\DE\SET_EXTERN -PSProvider FileSystem -Scope 'Global' -Persist


##### Outlook: Siemens Address Books
# https://manuals.siemens.com/useit/manual/outlook/en/mail-encryption-fails
Print-StatusInfo -Info 'Mapping Network Shares'

##### Shortcuts
Print-StatusInfo 'Creating Shortcuts'
Create-Shortcut -Name 'Brave - No Extensions' -Executable 'C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe' '--incognito --user-data-dir=D:\Tools\Brave\NoExtensions' -TargetFolder Desktop
Create-Shortcut -Name 'Brave - Orange' -Executable 'C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe' '--incognito --user-data-dir=D:\Tools\Brave\PenTestORANGE' -TargetFolder Desktop
Create-Shortcut -Name 'Brave - Blue' -Executable 'C:\Program Files\BraveSoftware\Brave-Browser\Application\brave.exe' '--incognito --user-data-dir=D:\Tools\Brave\PenTestBLUE' -TargetFolder Desktop
Create-Shortcut -Name 'Sysinternals Suite' -Executable 'D:\Tools\SysinternalsSuite' -TargetFolder Desktop
Create-Shortcut -Name 'Process Explorer' -Executable 'D:\Tools\SysinternalsSuite\procexp64.exe' -TargetFolder Desktop
Create-Shortcut -Name 'Process Monitor' -Executable 'D:\Tools\SysinternalsSuite\Procmon64.exe' -TargetFolder Desktop
Create-Shortcut -Name 'Autoruns' -Executable 'D:\Tools\SysinternalsSuite\Autoruns64.exe' -TargetFolder Desktop
Create-Shortcut -Name 'PuTTy' -Executable 'D:\Tools\PuTTY\PUTTY.EXE' -TargetFolder Desktop
#Create-Shortcut -Name 'PSCP' -Executable 'D:\Tools\PuTTY\PSCP.EXE' -TargetFolder Desktop
#Create-Shortcut -Name 'PSFTP' -Executable 'D:\Tools\PuTTY\PSFTP.EXE' -TargetFolder Desktop
Create-Shortcut -Name 'PAGEANT' -Executable 'D:\Tools\PuTTY\PAGEANT.EXE' -TargetFolder Desktop
Create-Shortcut -Name 'WinSCP' -Executable 'D:\Tools\WinSCP\WinSCP.exe' -TargetFolder Desktop
Create-Shortcut -Name 'Local Group Policy Editor' -Executable 'C:\Windows\System32\gpedit.msc' -TargetFolder Desktop
Create-Shortcut -Name 'Certificates - Current User' -Executable 'C:\Windows\System32\certmgr.msc' -TargetFolder Desktop
Create-Shortcut -Name 'Certificates - Local Machine' -Executable 'C:\Windows\System32\certlm.msc' -TargetFolder Desktop
Create-Shortcut -Name 'Event Viewer' -Executable 'C:\Windows\system32\eventvwr.msc' '/s' -TargetFolder Desktop


##### User specific Application Setup
#### Brave
#Copy-WithProgress -Source 'D:\Tools\Brave\Settings\Brave-Browser' -Destination (Join-Path -Path $HOME -ChildPath 'AppData\Local\BraveSoftware\Brave-Browser')
Copy-WithProgress -Source ('D:\Tools\Brave\Settings\' + $env:USERNAME + '\Brave-Browser') -Destination (Join-Path -Path $HOME -ChildPath 'AppData\Local\BraveSoftware\Brave-Browser')
