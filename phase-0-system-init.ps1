﻿##### https://gitlab.com/SheriffTexer/windows-setup

#Requires -RunAsAdministrator

##### Variables
$hive = 'Machine'
$Tools = "D:\ToolsWindows\"
$PSTools = "D:\ToolsWindows\PowerShell\"
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'
$url_profile_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/profile.ps1'


##### PowerShell Execution Policy
##### Set-ExecutionPolicy Bypass -Scope Process -Force
Set-ExecutionPolicy Unrestricted -Force


##### Import Helper Functions
$path_to_helper_functions_script = Join-Path -Path $PSTools -ChildPath 'helper_functions.ps1'
If (!(Test-Path $path_to_helper_functions_script -PathType Leaf))
{
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}
. $path_to_helper_functions_script


##### Set Environment Variables
[System.Environment]::SetEnvironmentVariable("Tools", $Tools, $hive)
[System.Environment]::SetEnvironmentVariable("PSTools", $PSTools, $hive)


##### Read Environment Variables
$env:Tools, $env:PSTools = Read-EnvironmentVariables


# Setting Chocolatey Environment Variables
#[Environment]::SetEnvironmentVariable("ChocolateyToolsLocation", $env:Tools, 'User') # not needed
#[Environment]::SetEnvironmentVariable("ChocolateyToolsLocation", $env:Tools, 'Machine') # not needed


Print-Banner -Info 'Windows OS Setup - Phase 0: System Init'


##### Extend PATH Environment Variable
${Path-to-Set-PathVariable.ps1} = Join-Path -Path ([Environment]::GetEnvironmentVariable("PSTools", 'Machine')) -ChildPath 'Set-PathVariable.ps1'
Invoke-WebRequest -Uri 'https://raw.githubusercontent.com/hahndorf/hacops/master/Set-PathVariable.ps1' -OutFile ${Path-to-Set-PathVariable.ps1}
##### Execute downloaded Script: Set-PathVariable.ps1
#& ${Path-to-Set-PathVariable.ps1} -NewLocation ([Environment]::GetEnvironmentVariable("Tools", 'Machine'))
& ${Path-to-Set-PathVariable.ps1} -NewLocation $env:PSTools


##### Windows Defender: Exclustion Paths
Add-MpPreference -ExclusionPath $env:Tools
Add-MpPreference -ExclusionPath C:\Users\Hannes\AppData\Local\Packages\KaliLinux*
Add-MpPreference -ExclusionPath C:\Users\z003b97u\AppData\Local\Packages\KaliLinux*


##### Debloat Script
Print-StatusInfo -Info 'Executing Windows Debloating Script'
${Path-to-Windows10SysPrepDebloater.ps1} = Join-Path -Path $env:PSTools -ChildPath 'Windows10SysPrepDebloater.ps1'
#Invoke-WebRequest -Uri 'https://raw.githubusercontent.com/Sycnex/Windows10Debloater/master/Windows10SysPrepDebloater.ps1' -OutFile ${Path-to-Windows10SysPrepDebloater.ps1}
#& ${Path-to-Windows10SysPrepDebloater.ps1}


##### Remove Cortana
Print-StatusInfo -Info 'Removing Cortana'
Get-AppxPackage -AllUsers Microsoft.549981C3F5F10 | Remove-AppxPackage


##### Remove Windows Search
Print-StatusInfo -Info 'Removing Windows Search'
Get-AppxPackage -Name Microsoft.Windows.Search | Remove-AppxPackage


##### Remove other Shit
Print-StatusInfo -Info 'Removing HP Packages'
Get-AppxPackage -Name '*.HPWorkWell' | Remove-AppxPackage
Get-AppxPackage -Name '*.HPSupportAssistant' | Remove-AppxPackage
Get-AppxPackage -Name '*.HPQuickDrop' | Remove-AppxPackage
Get-AppxPackage -Name '*.HPProgrammableKey' | Remove-AppxPackage
Get-AppxPackage -Name '*.HPPrivacySettings' | Remove-AppxPackage


##### Install the OpenSSH Client
Add-WindowsCapability -Online -Name OpenSSH.Client~~~~0.0.1.0


##### Enable Hyper-V
Print-StatusInfo -Info 'Enabling Hyper-V'
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Hyper-V -All -NoRestart


##### Enable Edge App Sandbox
Print-StatusInfo -Info 'Enabling Edge App Sandbox'
Enable-WindowsOptionalFeature -Online -FeatureName Windows-Defender-ApplicationGuard -NoRestart


##### Enable Windows Subsystem for Linux
##### Should NOT be needed, according to: https://docs.microsoft.com/en-us/windows/wsl/install#install
Print-StatusInfo -Info 'Enabling Windows Subsystem for Linux'
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux -NoRestart
Enable-WindowsOptionalFeature -Online -FeatureName VirtualMachinePlatform -NoRestart


##### Install Linux Kernel Update Package
Print-StatusInfo -Info 'Installing Linux Kernel Update Package'
${Path-to-wsl_update_x64.msi} = Join-Path -Path $env:TEMP -ChildPath 'wsl_update_x64.msi'
Invoke-WebRequest -Uri 'https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi' -OutFile ${Path-to-wsl_update_x64.msi} -UseBasicParsing
msiexec /i ${Path-to-wsl_update_x64.msi} /quiet


##### Device Guard/Credential Guard
##### Script URL: https://docs.microsoft.com/en-us/windows/security/identity-protection/credential-guard/dg-readiness-tool
Print-StatusInfo -Info 'Enabling Credential Guard & Device Guard'
${Path-to-DG_Readiness_Tool.ps1} = Join-Path -Path $env:PSTools -ChildPath 'DG_Readiness_Tool.ps1'
Copy-Item (Join-Path -Path $PSScriptRoot -ChildPath 'DG_Readiness_Tool.ps1') -Destination ${Path-to-DG_Readiness_Tool.ps1}
& ${Path-to-DG_Readiness_Tool.ps1} -Ready
& ${Path-to-DG_Readiness_Tool.ps1} -Enable


##### Continue with next Script
WaitFor-UserInput -Info 'Please restart and continue with script' -UserInput 'phase-1-applications.ps1' -Symbol '[>]'

##### Restart
Restart-Computer -Confirm