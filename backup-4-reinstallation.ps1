﻿##### https://gitlab.com/SheriffTexer/windows-setup

#Requires -RunAsAdministrator


##### Environment Variables
$env:PSTools = [System.Environment]::GetEnvironmentVariable('PSTools', 'Machine') # locally scoped to current PowerShell session


##### Import Helper Functions
${Path-to-helper-functions.ps1} = Join-Path -Path $env:PSTools -ChildPath 'helper_functions.ps1'
Invoke-WebRequest -Uri 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1' -OutFile ${Path-to-helper-functions.ps1}
. ${Path-to-helper-functions.ps1}


Print-Banner -Info 'Windows OS Setup - Settings Backup'


##### PuTTY: Settings
Print-StatusInfo -Info 'Backing up PuTTY Settings'
reg export HKCU\Software\SimonTatham (Join-Path -Path $env:Tools -ChildPath "\PuTTY\Settings\putty_settings.reg")


##### Brave: User Profile
Print-StatusInfo -Info 'Backing up Brave User Profile'
Copy-WithProgress -Source (Join-Path -Path $HOME -ChildPath 'AppData\Local\BraveSoftware\Brave-Browser') -Destination ('D:\Tools\Brave\Settings\' + $env:USERNAME + '\Brave-Browser')