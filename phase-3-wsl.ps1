﻿##### https://gitlab.com/SheriffTexer/windows-setup

#Requires -RunAsAdministrator

##### Variables
$PSTools = "D:\ToolsWindows\PowerShellTools\"
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'

##### Import Helper Functions
$path_to_helper_functions_script = Join-Path -Path $PSTools -ChildPath 'helper_functions.ps1'
If (!(Test-Path $path_to_helper_functions_script -PathType Leaf))
{
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}
. $path_to_helper_functions_script


##### Read Environment Variables
$env:Tools, $env:PSTools = Read-EnvironmentVariables


Print-Banner -Info 'Windows OS Setup - Phase 3: WSL'


##### Receive Updates for other Microsoft Products when you update Windows
$ServiceManager = New-Object -ComObject "Microsoft.Update.ServiceManager"
$ServiceManager.ClientApplicationID = "My App"
$ServiceManager.AddService2( "7971f918-a847-4430-9279-4a52d1efe18d",7,"")

##### Update Windows Subsystem for Linux
# https://devblogs.microsoft.com/commandline/install-wsl-with-a-single-command-now-available-in-windows-10-version-2004-and-higher/
# https://support.microsoft.com/en-us/topic/july-29-2021-kb5004296-os-builds-19041-1151-19042-1151-and-19043-1151-preview-6aba536a-6ed2-41cb-bc3d-3980e8693cc4
Print-StatusInfo -Info 'Updating Windows Subsystem for Linux'
wsl --update
wsl --shutdown
wsl --set-default-version 2


##### Windows Subsystem for Linux
#### Kali Linux
### Window Settings: Properties
## Font: Consolas
## Size: 16
Print-StatusInfo -Info 'Installing Kali Linux'
##### ONLY 'wsl --install' should be needed, according to: https://docs.microsoft.com/en-us/windows/wsl/install#install


### Kali Installation Method 1: wsl --install
wsl --install -d kali-linux
#WaitFor-UserInput -Info 'Please specify the following data in the new Kali window:' -UserInput 'Username/Password'
#Pause


### Kali Installation Method 2: Install from Microsoft Store
#${Path-to-Kali-Appx-Package} = Download-Appx-From-Store -Url "https://www.microsoft.com/en-us/p/kali-linux/9pkr34tncv07"
#Add-AppxPackage -Path ${Path-to-Kali-Appx-Package}


# check Kali version
#$KaliVersion = kali run  """cat /etc/os-release | grep VERSION= | cut -f 2 -d '\`"' | cut -f 1 -d '.' """
#If ($KaliVersion -lt 2021)
#{
#    Print-StatusInfo -Info "Kali Version $KaliVersion of provided Image is outdated! Please check WSL Version and try again." -Symbol '[!]'
#    Return
#}
#kali config --default-user root
#kali config --default-user kali


### Kali Installation Method 3: download & extract package
#${Path-to-kali-package} = Join-Path -Path $env:TEMP -ChildPath 'kali-new.zip'
#Invoke-WebRequest -Uri https://aka.ms/wsl-kali-linux-new -OutFile ${Path-to-kali-package} -UseBasicParsing
#Invoke-WebRequest -Uri https://aka.ms/wsl-kali-linux -OutFile kali-old.appx -UseBasicParsing
#move .\'kali-new.appx' .\kali-new.zip -Force
#Expand-Archive -Path ${Path-to-kali-package} -DestinationPath 'D:\WSL\distros\kali-new' -Force
#move D:\WSL\distros\kali-new\DistroLauncher-Appx_1.1.9.0_x64.appx D:\WSL\distros\kali-new\DistroLauncher-Appx_1.1.9.0_x64.zip -Force
#Expand-Archive -Path D:\WSL\distros\kali-new\DistroLauncher-Appx_1.1.9.0_x64.zip -DestinationPath 'D:\WSL\distros\kali-new\DistroLauncher-Appx_1.1.9.0_x64' -Force
#wsl --import kali-linux D:\WSL\kali-linux D:\WSL\distros\kali-new\DistroLauncher-Appx_1.1.9.0_x64\install.tar.gz


### Kali Installation Method 4: chocolatey package
#choco install --confirm wsl-kalilinux # via choco package


##### Fixing DNS Issues for Siemens Standard Client
#kali run "echo 'rm /etc/resolv.conf' > /root/fix-nameserver.sh"
#kali run "echo 'echo nameserver 8.8.8.8 > /etc/resolv.conf' >> /root/fix-nameserver.sh"
#kali run "echo 'chattr +i /etc/resolv.conf' >> /root/fix-nameserver.sh"
#kali run "echo 'echo [network] > /etc/wsl.conf' >> /root/fix-nameserver.sh"
#kali run "echo 'echo generateResolvConf = false >> /etc/wsl.conf' >> /root/fix-nameserver.sh"
#kali run chmod +x /root/fix-nameserver.sh
#kali run /root/fix-nameserver.sh

##### Get current Key Ring
#kali sudo run wget --no-check-certificate https://http.kali.org/kali/pool/main/k/kali-archive-keyring/kali-archive-keyring_2020.2_all.deb -O ~/kali-archive-keyring_2020.2_all.deb
#kali sudo run apt install ~/kali-archive-keyring_2020.2_all.deb

##### Kali Linux: Update/Upgrade
kali run "apt update && apt full-upgrade -y"
kali run "apt install -y kali-linux-core"
kali run "apt install -y kali-desktop-core"
WaitFor-UserInput -Info 'Please manually open Kali and run the following command:' -UserInput 'apt install -y kali-win-kex'
Pause
#kali run "apt install -y kali-win-kex" # Kali Desktop Experience for WSL
WaitFor-UserInput -Info 'Please manually open Kali and run the following command:' -UserInput 'apt install -y kali-linux-default'
Pause
kali run "apt install -y virtualenv"
kali run "apt install -y virtualenvwrapper"
kali run "apt install -y awscli"

##### Linux OS Setup Script
Print-StatusInfo 'Configuring Linux OS'
kali run cd '$HOME' '&&' git clone https://gitlab.com/SheriffTexer/linux-setup.git
#kali run cd '$HOME/linux-setup' '&&' chmod +x linux-os-setup.sh
#kali run cd '$HOME' '&&' cd linux-setup '&&' ./linux-os-setup.sh
kali run ln --force --symbolic '$HOME/linux-setup/bashrc' '$HOME/.bashrc'
kali run ln --force --symbolic '$HOME/linux-setup/zshrc' '$HOME/.zshrc'
kali run ln --force --symbolic '$HOME/linux-setup/vimrc' '$HOME/.vimrc'
kali run chsh -s /bin/zsh root
kali run chsh -s /bin/zsh kali
