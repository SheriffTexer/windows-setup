﻿##### https://gitlab.com/SheriffTexer/windows-setup

#Requires -RunAsAdministrator

##### Variables
$PSTools = "D:\ToolsWindows\PowerShell\"
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'

##### Import Helper Functions
$path_to_helper_functions_script = Join-Path -Path $PSTools -ChildPath 'helper_functions.ps1'
If (!(Test-Path $path_to_helper_functions_script -PathType Leaf))
{
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}
. $path_to_helper_functions_script


##### Read Environment Variables
$env:Tools, $env:PSTools = Read-EnvironmentVariables


Print-Banner -Info 'Windows OS Setup - Phase 1: Application Installation'


##### RSAT: Active Dirctory Domain Services and Lightweigth Directory Services Tools
Print-StatusInfo -Info 'Installing RSAT: AD Tools'
Get-WindowsCapability -Name RSAT.ActiveDirectory* -Online | Add-WindowsCapability -Online


##### Chocolatey Package Manager: Installation
Print-StatusInfo -Info 'Installing Chocolatey Package Manager'
[System.Net.ServicePointManager]::SecurityProtocol = [System.Net.ServicePointManager]::SecurityProtocol -bor 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://community.chocolatey.org/install.ps1'))


##### Scheduled Task: Choco Upgrade All
# See if choco.exe is available. If not, stop execution
$chocoCmd = Get-Command –Name 'choco' –ErrorAction SilentlyContinue –WarningAction SilentlyContinue | Select-Object –ExpandProperty Source
If ($chocoCmd -eq $null)
{
    Print-StatusInfo -Info 'Chocolatey was not found on the system!' -Symbol '[!]'
}
Else
{
    Print-StatusInfo -Info 'Creating Scheduled Task: Choco Upgrad All'
    $taskAction = New-ScheduledTaskAction –Execute $chocoCmd –Argument 'upgrade all -y'
    $taskTrigger = New-ScheduledTaskTrigger –AtStartup
    $taskUserPrincipal = New-ScheduledTaskPrincipal –UserId 'SYSTEM'
    $taskSettings = New-ScheduledTaskSettingsSet -AllowStartIfOnBatteries -Compatibility `Win8
    Register-ScheduledTask –TaskName 'Choco Upgrade All at Startup' –Action $taskAction –Principal $taskUserPrincipal –Trigger $taskTrigger –Settings $taskSettings –Force
}


##### Chocolatey: Options
choco feature enable -n=useRememberedArgumentsForUpgrades # to have Chocolatey remember parameters on upgrade
choco feature enable -name=exitOnRebootDetected
choco feature enable -name=allowGlobalConfirmation

##### Automatic Installation of Packages/Applications
Print-StatusInfo -Info 'Installing Chocolatey Packages'
choco install --confirm 7zip
choco install --confirm adobereader
choco install --confirm burp-suite-pro-edition
choco install --confirm git
choco install --confirm GoogleChrome
choco install --confirm greenshot
choco install --confirm microsoft-windows-terminal
Invoke-Expression "choco install --confirm openvpn --params '/InstallDir:$($env:Tools)\OpenVPN' --package-parameters='/Gui /EasyRsa /TapDriver /WintunDriver /OpenSSL'"
choco install --confirm pdftk
choco install --confirm powertoys
Invoke-Expression "choco install --confirm python3 --params '/InstallDir:$($env:Tools)\Python3'"
choco install --confirm pwsh --install-arguments='"ADD_EXPLORER_CONTEXT_MENU_OPENPOWERSHELL=1 REGISTER_MANIFEST=1"' --packageparameters '"/CleanUpPath"'   # PowerShell 7
choco install --confirm signal
Invoke-Expression "choco install --confirm sysinternals --params '/InstallDir:$($env:Tools)\SysinternalsSuite'"
Invoke-Expression "choco install --confirm tor-browser --params '/InstallDir:$($env:Tools)\Tor-Browser'"
choco install --confirm TortoiseGit
Invoke-Expression "choco install --confirm vim --params '/InstallDir:$($env:Tools)\Vim'"
choco install --confirm vlc
choco install --confirm vmwareworkstation
choco install --confirm WinPcap
choco install --confirm wireshark



##### Manual Application Installation
Print-StatusInfo -Info 'Installing other Applications'
${Path-to-Brave-Installer} = Join-Path -Path $env:TEMP -ChildPath 'brave_installer-x64.exe'
# updater of offline installer is broken:
#Invoke-WebRequest -Uri 'https://brave-browser-downloads.s3.brave.com/latest/brave_installer-x64.exe' -OutFile ${Path-to-Brave-Installer} #  offline installer
Invoke-WebRequest -Uri 'https://laptop-updates.brave.com/latest/winx64' -OutFile ${Path-to-Brave-Installer} # online installer
& ${Path-to-Brave-Installer} #--install --silent --system-level




##### Chocolatey: Remember Package Features
choco feature enable -name=useRememberedArgumentsForUpgrades



##### Continue with next Script
WaitFor-UserInput -Info 'Please restart and continue with script' -UserInput 'phase-2-user-customization.ps1' -Symbol '[>]'

##### Restart
Restart-Computer -Confirm