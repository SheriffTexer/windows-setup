# https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_profiles

##### Variables
$PSTools = "D:\ToolsWindows\PowerShell\"
$url_helper_functions_script = 'https://gitlab.com/SheriffTexer/windows-setup/raw/main/helper_functions.ps1'

##### Import Helper Functions
$path_to_helper_functions_script = Join-Path -Path $PSTools -ChildPath 'helper_functions.ps1'
If (!(Test-Path $path_to_helper_functions_script -PathType Leaf))
{
    Invoke-WebRequest -Uri $url_helper_functions_script -OutFile $path_to_helper_functions_script
}
. $path_to_helper_functions_script


##### Read Environment Variables
$env:Tools, $env:PSTools = Read-EnvironmentVariables


# Change window title
$hostversion = "$($PSVersionTable.PSVersion.Major).$($PSVersionTable.PSVersion.Minor)"
If (Is-Administrator)
{
    $Host.UI.RawUI.WindowTitle = "Administrator: PowerShell $hostversion"
}
Else
{
    $Host.UI.RawUI.WindowTitle = "PowerShell $hostversion"
}


# Change the foreground and background colors for console and ISE:
If ($host.Name -like "*ISE*")
{
    $psISE.Options.ConsolePaneBackgroundColor = "black"
    $psISE.Options.ConsolePaneTextBackgroundColor = "black"
    $psISE.Options.ConsolePaneForegroundColor = "white"
    $psISE.Options.FontName = "Lucida Console"
    $psISE.Options.FontSize = 10
}
#Else
#{
#    [system.console]::set_foregroundcolor("white") 
#    [system.console]::set_backgroundcolor("black")
#}


# Change the color of the command prompt to yellow:
Function Prompt 
{
    Write-Host "$(get-location)>" -nonewline -foregroundcolor yellow
    Return ' '  #Needed to remove the extra "PS"
}


# Feel free to add more functions, aliases, drives, etc:
function tt { cd c:\temp }
function hh ( $term ) { get-help $term -full | more }
function nn ( $path ) { notepad.exe $path } 


# Switch to my PowerShell folder:
cd $env:PSTools


# The verbose test here is just for the SEC505 setup script,
# you can delete it if you wish after the course finishes:
if ($VerbosePreference -ne "Continue") { cls ; dir }


# Chocolatey profile
$ChocolateyProfile = "$env:ChocolateyInstall\helpers\chocolateyProfile.psm1"
if (Test-Path($ChocolateyProfile)) {
  Import-Module "$ChocolateyProfile"
}
